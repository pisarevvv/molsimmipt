# Описание
В этой работе изучаются различные методы термостатирования для молекулярной динамики.

В качестве примера рассматривается система из 125 частиц, взаимодействующих со сглаженным потенциалом Леннард-Джонса
```math
U_{LJ}(r) = \begin{cases} 4\varepsilon \left[\left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^6\right], r \leq r_{in}, \\
ar^3 + br^2 + cr + d, r_{in} < r \leq r_{out} \\
0, r > r_{out}\end{cases}
```
Коэффициенты сглаживающего многочлена подбираются так, чтобы потенциал и его производная непрерывно сшивались с нулём на интервале от $`r_{in}`$ до $`r_{out}`$.

Рассмотрены следующие методы термостатирования:
* термостат Берендсена
* ланжевеновская динамика
* цепочка Нозе-Гувера

## Процедура расчета

В начале расчета система приводится к кинетической температуре $`T = 1.0`$ простым масштабированием скоростей, после чего запускаются равновесные расчеты с указанными термостатами для той же температуры $`T = 1.0`$.

Для всех термостатов задано характерное время установления температуры 0.2 LJ единицы.

В качестве "эталонного" расчета также добавлено моделирование той же системы методом Монте-Карло (при помощи команды `fix gcmc`). Для сравнения методов вычисляется среднее значение вириального давления при заданной плотности и параметрах расчета. Количество циклов в методе Монте-Карло взято равным количеству шагов в методе молекулярной динамики.

Давление усредняется по `nintervals` точкам, взятым с интервалом 500 шагов МД. Значением переменной `nintervals` во входном файле меняется число этих точек и, соответственно, длительность расчета.

## Файлы
Входные файлы:
* `in.melt` - входной файл.

Выходные файлы:
* `log.berendsen.txt` - информация о расчете с термостатом Берендсена
* `log.langevin.txt` - информация о расчете с термостатом Ланжевена
* `log.mdnvt.txt` - информация о расчете с термостатом Нозе-Гувера
* `log.mcnvt.txt` - информация о расчете методом Монте-Карло по алгоритму Метрополиса

## Запуск
`lmp_* -in in.melt` - основной запуск

Дополнительные параметры:
* `-var nintervals ⟨значение⟩` - изменение числа интервалов, по которым проводится усреднение давления (меняет также число шагов в расчете)
* `-var mcmaxdispl ⟨значение⟩` - изменение максимальной величины пробного смещения в расчете методом Монте-Карло; влияет на вероятность принятия пробных шагов
* `-var temperature ⟨значение⟩` - изменение температуры, поддерживаемой термостатами (по умолчанию равна 1.0)

## Задание
1. Запустить входной файл `in.melt`.
2. В выходных файлах посмотреть усредненные величины вириального давления, полученные в разных методах. Насколько отличаются эти величины?
3. С помощью программы постобработки или редактора таблиц оценить погрешность среднего значения вириального давления. Являются ли различия между разными методами термостатирования статистически значимыми?
4. Для выходных файлов построить зависимости полной энергии от времени. Что можно сказать о различии флуктуаций энергии для термостатов Берендсена, Ланжевена и Нозе-Гувера?
5. Найти в лог-файлах столбец, соответствующий вероятности принятия шагов в методе Монте-Карло. Чему она равна для значений, установленных по умолчанию?
6. Сравнить время расчета методом молекулярной динамики и методом Монте-Карло. Предложить возможные причины различия.

## Вопросы
1. Как соотносятся флуктуации полной энергии в каноническом ансамбле с теплоемкостью системы?
2. Чему равна флуктуация кинетической температуры в каноническом ансамбле? Выполняется ли это для всех термостатов в данной работе?
3. Верно ли, что средние значения вириального давления в разных термостатах приближаются друг к другу по мере удлинения расчета?
4. Как меняется вероятность принятия шагов в методе Монте-Карло с изменением максимально допустимой величины пробного смещения?